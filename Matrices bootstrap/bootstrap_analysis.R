#Analysis of the convergence of parameters for diff and diff estimation with bootstrap

library("dplyr")
library("stargazer")
library("ggplot2")
library("extrafont")
library("matrixStats")

# Import all data sets at once

#path <- "C:/Users/Vincent Vergnat/Dropbox/diff-diff/diff-in-diff/Matrices bootstrap/"
path <- "~/Dropbox/Documents/Work/Thesis/diff in diff fertility education/Matrices bootstrap/"
files <- list.files(path=path, pattern="*.csv")
for(file in files)
{
    perpos <- which(strsplit(file, "")[[1]]==".")
    assign(
        gsub(" ","",substr(file, 1, perpos-1)), 
        read.csv2(paste(path,file,sep=""), row.names=1))
}

xaxis <- seq(1,1000,by=1)

hwage_w <- rbind(hwage_matrix_w_2000_2002_2004,
                 hwage_matrix_w_2000_2002_2006,
                 hwage_matrix_w_2000_2002_2010)

hwage_m <- rbind(hwage_matrix_m_2000_2002_2004,
                 hwage_matrix_m_2000_2002_2006,
                 hwage_matrix_m_2000_2002_2010)

dwage_w <- rbind(dwage_matrix_w_2000_2002_2004,
                 dwage_matrix_w_2000_2002_2006,
                 dwage_matrix_w_2000_2002_2010)

dwage_m <- rbind(dwage_matrix_m_2000_2002_2004,
                 dwage_matrix_m_2000_2002_2006,
                 dwage_matrix_m_2000_2002_2010)

nbheur_w <- rbind(nbheu_matrix__w_2000_2002_2004,
                 nbheu_matrix__w_2000_2002_2006,
                 nbheu_matrix__w_2000_2002_2010)

nbheur_m <- rbind(nbheu_matrix__m_2000_2002_2004,
                 nbheu_matrix__m_2000_2002_2006,
                 nbheu_matrix__m_2000_2002_2010)

# Create graphs for women 
# Hourly wages
# Dependent variable hwage, 2 years later, women
pdf(file="boot_hwage_w_2.pdf")
par(mfrow = c(2, 2))
par(mar=c(6,4,4,1), xpd=T)
hwage_w[1,][hwage_w[1,] < -1.5] <- -1.5
plot(xaxis, hwage_w[1, ], type="l", 
     ylab="", xlab="", main="Low education", ylim=c(-1.5,1.5))

plot(xaxis, hwage_w[2, ], type="l", 
     ylab="", xlab="", main="High school only", ylim=c(-1.5,1.5))

plot(xaxis, hwage_w[3, ], type="l", 
     ylab="", xlab="", main="2 to 3 years of higher education", ylim=c(-1.5,1.5))

hwage_w[4,][hwage_w[4,] < -1.5] <- -1.5
plot(xaxis, hwage_w[4, ], type="l", 
     ylab="", xlab="", main="4 to 8 years of higher education", ylim=c(-1.5,1.5))

mtext("Number of simulations", side=1, line=-1.5, outer=T)
mtext("Parameter value", side=2, line=-1.5, outer=T)
dev.off()

# Hourly wages
# Dependent variable hwage, 4 years later, women
pdf(file="boot_hwage_w_4.pdf")
par(mfrow = c(2, 2))
par(mar=c(6,4,4,1), xpd=T)
plot(xaxis, hwage_w[5, ], type="l", 
     ylab="", xlab="", main="Low education", ylim=c(-2.5,5.5))

plot(xaxis, hwage_w[6, ], type="l", 
     ylab="", xlab="", main="High school only", ylim=c(-2.5,5.5))

plot(xaxis, hwage_w[7, ], type="l", 
     ylab="", xlab="", main="2 to 3 years of higher education", ylim=c(-2.5,5.5))

plot(xaxis, hwage_w[8, ], type="l", 
     ylab="", xlab="", main="4 to 8 years of higher education", ylim=c(-2.5,5.5))

mtext("Number of simulations", side=1, line=-1.5, outer=T)
mtext("Parameter value", side=2, line=-1.5, outer=T)

dev.off()

# Hourly wages
# Dependent variable hwage, 8 years later, women
pdf(file="boot_hwage_w_8.pdf")
par(mfrow = c(2, 2))
par(mar=c(6,4,4,1), xpd=T)
hwage_w[9,][hwage_w[9,] > 1.5] <- 1.5
hwage_w[9,][hwage_w[9,] < -2] <- -2
plot(xaxis, hwage_w[9, ], type="l", 
     ylab="", xlab="", main="Low education", ylim=c(-2,1.5))


plot(xaxis, hwage_w[10, ], type="l", 
     ylab="", xlab="", main="High school only", ylim=c(-2,1.5))

hwage_w[11,][hwage_w[11,] > 1.5] <- 1.5
hwage_w[11,][hwage_w[11,] < -2] <- -2
plot(xaxis, hwage_w[11, ], type="l", 
     ylab="", xlab="", main="2 to 3 years of higher education", ylim=c(-2,1.5))

hwage_w[12,][hwage_w[12,] > 1.5] <- 1.5
hwage_w[12,][hwage_w[12,] < -2] <- -2
plot(xaxis, hwage_w[12, ], type="l", 
     ylab="", xlab="", main="4 to 8 years of higher education", ylim=c(-2,1.5))

mtext("Number of simulations", side=1, line=-1.5, outer=T)
mtext("Parameter value", side=2, line=-1.5, outer=T)
dev.off()

# Daily wages
# Dependent variable dwage, 2 years later, women
pdf(file="boot_dwage_w_2.pdf")
par(mfrow = c(2, 2))
par(mar=c(6,4,4,1), xpd=T)
dwage_w[1,][dwage_w[1,] > 5] <- 5
dwage_w[1,][dwage_w[1,] < -10] <- -10
plot(xaxis, dwage_w[1, ], type="l", 
     ylab="", xlab="", main="Low education", ylim=c(-10,5))

plot(xaxis, dwage_w[2, ], type="l", 
     ylab="", xlab="", main="High school only", ylim=c(-10,5))

plot(xaxis, dwage_w[3, ], type="l", 
     ylab="", xlab="", main="2 to 3 years of higher education", ylim=c(-10,5))

dwage_w[4,][dwage_w[4,] > 5] <- 5
dwage_w[4,][dwage_w[4,] < -10] <- -10
plot(xaxis, dwage_w[4, ], type="l", 
     ylab="", xlab="", main="4 to 8 years of higher education", ylim=c(-10,5))

mtext("Number of simulations", side=1, line=-1.5, outer=T)
mtext("Parameter value", side=2, line=-1.5, outer=T)
dev.off()

# Daily wages
# Dependent variable dwage, 4 years later, women
pdf(file="boot_dwage_w_4.pdf")
par(mfrow = c(2, 2))
par(mar=c(6,4,4,1), xpd=T)
dwage_w[5,][dwage_w[5,] > 5.5] <- 5.5
dwage_w[5,][dwage_w[5,] < -5.5] <- -5.5
plot(xaxis, dwage_w[5, ], type="l", 
     ylab="", xlab="", main="Low education", ylim=c(-5.5,5.5))

plot(xaxis, dwage_w[6, ], type="l", 
     ylab="", xlab="", main="High school only", ylim=c(-5.5,5.5))

dwage_w[7,][dwage_w[7,] > 5.5] <- 5.5
dwage_w[7,][dwage_w[7,] < -5.5] <- -5.5
plot(xaxis, dwage_w[7, ], type="l", 
     ylab="", xlab="", main="2 to 3 years of higher education", ylim=c(-5.5,5.5))


dwage_w[8,][dwage_w[8,] > 5.5] <- 5.5
dwage_w[8,][dwage_w[8,] < -5.5] <- -5.5
plot(xaxis, dwage_w[8, ], type="l", 
     ylab="", xlab="", main="4 to 8 years of higher education", ylim=c(-5.5,5.5))

mtext("Number of simulations", side=1, line=-1.5, outer=T)
mtext("Parameter value", side=2, line=-1.5, outer=T)

dev.off()


# Daily wages
# Dependent variable dwage, 8 years later, women
pdf(file="boot_dwage_w_8.pdf")
par(mfrow = c(2, 2))
par(mar=c(6,4,4,1), xpd=T)
dwage_w[9,][dwage_w[9,] > 15] <- 15
dwage_w[9,][dwage_w[9,] < -15] <- -15
plot(xaxis, dwage_w[9, ], type="l", 
     ylab="", xlab="", main="Low education", ylim=c(-15,15))


plot(xaxis, dwage_w[10, ], type="l", 
     ylab="", xlab="", main="High school only", ylim=c(-15,15))

dwage_w[11,][dwage_w[11,] > 15] <- 15
dwage_w[11,][dwage_w[11,] < -15] <- -15
plot(xaxis, dwage_w[11, ], type="l", 
     ylab="", xlab="", main="2 to 3 years of higher education", ylim=c(-15,15))

dwage_w[12,][dwage_w[12,] > 15] <- 15
dwage_w[12,][dwage_w[12,] < -15] <- -15
plot(xaxis, dwage_w[12, ], type="l", 
     ylab="", xlab="", main="4 to 8 years of higher education", ylim=c(-15,15))

mtext("Number of simulations", side=1, line=-1.5, outer=T)
mtext("Parameter value", side=2, line=-1.5, outer=T)

dev.off()

# Number hours
# Dependent variable nbheur, 2 years later, women

pdf(file="boot_nbheur_w_2.pdf")
par(mfrow = c(2, 2))
par(mar=c(6,4,4,1), xpd=T)
nbheur_w[1,][nbheur_w[1,] > 300] <- 300
nbheur_w[1,][nbheur_w[1,] < -300] <- -300
plot(xaxis, nbheur_w[1, ], type="l", 
     ylab="", xlab="", main="Low education", ylim=c(-300,300))

plot(xaxis, nbheur_w[2, ], type="l", 
     ylab="", xlab="", main="High school only", ylim=c(-300,300))

plot(xaxis, nbheur_w[3, ], type="l", 
     ylab="", xlab="", main="2 to 3 years of higher education", ylim=c(-300,300))

nbheur_w[4,][nbheur_w[4,] > 300] <- 300
nbheur_w[4,][nbheur_w[4,] < -300] <- -300
plot(xaxis, nbheur_w[4, ], type="l", 
     ylab="", xlab="", main="4 to 8 years of higher education", ylim=c(-300,300))

mtext("Number of simulations", side=1, line=-1.5, outer=T)
mtext("Parameter value", side=2, line=-1.5, outer=T)
dev.off()

# Number hours
# Dependent variable nbheur, 4 years later, women
pdf(file="boot_nbheur_w_4.pdf")
par(mfrow = c(2, 2))
par(mar=c(6,4,4,1), xpd=T)
nbheur_w[5,][nbheur_w[5,] > 400] <- 400
nbheur_w[5,][nbheur_w[5,] < -300] <- -300
plot(xaxis, nbheur_w[5, ], type="l", 
     ylab="", xlab="", main="Low education", ylim=c(-300,400))

plot(xaxis, nbheur_w[6, ], type="l", 
     ylab="", xlab="", main="High school only", ylim=c(-300,400))

nbheur_w[7,][nbheur_w[7,] > 400] <- 400
nbheur_w[7,][nbheur_w[7,] < -300] <- -300
plot(xaxis, nbheur_w[7, ], type="l", 
     ylab="", xlab="", main="2 to 3 years of higher education", ylim=c(-300,400))


nbheur_w[8,][nbheur_w[8,] > 400] <- 400
nbheur_w[8,][nbheur_w[8,] < -300] <- -300
plot(xaxis, nbheur_w[8, ], type="l", 
     ylab="", xlab="", main="4 to 8 years of higher education", ylim=c(-300,400))

mtext("Number of simulations", side=1, line=-1.5, outer=T)
mtext("Parameter value", side=2, line=-1.5, outer=T)
dev.off()

# Number hours
# Dependent variable nbheur, 8 years later, women
pdf(file="boot_nbheur_w_8.pdf")
par(mfrow = c(2, 2))
par(mar=c(6,4,4,1), xpd=T)
nbheur_w[9,][nbheur_w[9,] > 200] <- 200
nbheur_w[9,][nbheur_w[9,] < -900] <- -900
plot(xaxis, nbheur_w[9, ], type="l", 
     ylab="", xlab="", main="Low education", ylim=c(-900,200))


plot(xaxis, nbheur_w[10, ], type="l", 
     ylab="", xlab="", main="High school only", ylim=c(-900,200))

nbheur_w[11,][nbheur_w[11,] > 200] <- 200
nbheur_w[11,][nbheur_w[11,] < -900] <- -900
plot(xaxis, nbheur_w[11, ], type="l", 
     ylab="", xlab="", main="2 to 3 years of higher education", ylim=c(-900,200))

nbheur_w[12,][nbheur_w[12,] > 200] <- 200
nbheur_w[12,][nbheur_w[12,] < -900] <- -900
plot(xaxis, nbheur_w[12, ], type="l", 
     ylab="", xlab="", main="4 to 8 years of higher education", ylim=c(-900,200))

mtext("Number of simulations", side=1, line=-1.5, outer=T)
mtext("Parameter value", side=2, line=-1.5, outer=T)

dev.off()

#####################################################################################################
# Create graphs for men 
# Hourly wages
# Dependent variable hwage, 2 years later, men
pdf(file="boot_hwage_m_2.pdf")
par(mfrow = c(2, 2))
par(mar=c(6,4,4,1), xpd=T)
plot(xaxis, hwage_m[1, ], type="l", 
     ylab="", xlab="", main="Low education", ylim=c(-2.5,3.5))

plot(xaxis, hwage_m[2, ], type="l", 
     ylab="", xlab="", main="High school only", ylim=c(-2.5,3.5))

plot(xaxis, hwage_m[3, ], type="l", 
     ylab="", xlab="", main="2 to 3 years of higher education", ylim=c(-2.5,3.5))

plot(xaxis, hwage_m[4, ], type="l", 
     ylab="", xlab="", main="4 to 8 years of higher education", ylim=c(-2.5,3.5))

mtext("Number of simulations", side=1, line=-1.5, outer=T)
mtext("Parameter value", side=2, line=-1.5, outer=T)
dev.off()

# Hourly wages
# Dependent variable hwage, 4 years later, men
pdf(file="boot_hwage_m_4.pdf")
par(mfrow = c(2, 2))
par(mar=c(6,4,4,1), xpd=T)
plot(xaxis, hwage_m[5, ], type="l", 
     ylab="", xlab="", main="Low education", ylim=c(-2.5,6.5))

plot(xaxis, hwage_m[6, ], type="l", 
     ylab="", xlab="", main="High school only", ylim=c(-2.5,6.5))

plot(xaxis, hwage_m[7, ], type="l", 
     ylab="", xlab="", main="2 to 3 years of higher education", ylim=c(-2.5,6.5))

plot(xaxis, hwage_m[8, ], type="l", 
     ylab="", xlab="", main="4 to 8 years of higher education", ylim=c(-2.5,6.5))

mtext("Number of simulations", side=1, line=-1.5, outer=T)
mtext("Parameter value", side=2, line=-1.5, outer=T)
dev.off()

# Hourly wages
# Dependent variable hwage, 8 years later, men
pdf(file="boot_hwage_m_8.pdf")
par(mfrow = c(2, 2))
par(mar=c(6,4,4,1), xpd=T)
hwage_m[9,][hwage_m[9,] > 3.5] <- 3.5
hwage_m[9,][hwage_m[9,] < -2] <- -2
plot(xaxis, hwage_m[9, ], type="l", 
     ylab="", xlab="", main="Low education", ylim=c(-2,3.5))


plot(xaxis, hwage_m[10, ], type="l", 
     ylab="", xlab="", main="High school only", ylim=c(-2,3.5))

hwage_m[11,][hwage_m[11,] > 3.5] <- 3.5
hwage_m[11,][hwage_m[11,] < -2] <- -2
plot(xaxis, hwage_m[11, ], type="l", 
     ylab="", xlab="", main="2 to 3 years of higher education", ylim=c(-2,3.5))

hwage_m[12,][hwage_m[12,] > 3.5] <- 3.5
hwage_m[12,][hwage_m[12,] < -2] <- -2
plot(xaxis, hwage_m[12, ], type="l", 
     ylab="", xlab="", main="4 to 8 years of higher education", ylim=c(-2,3.5))

mtext("Number of simulations", side=1, line=-1.5, outer=T)
mtext("Parameter value", side=2, line=-1.5, outer=T)
dev.off()

# Daily wages
# Dependent variable dwage, 2 years later, men
pdf(file="boot_dwage_m_2.pdf")
par(mfrow = c(2, 2))
par(mar=c(6,4,4,1), xpd=T)
dwage_m[1,][dwage_m[1,] > 10] <- 10
dwage_m[1,][dwage_m[1,] < -5] <- -5
plot(xaxis, dwage_m[1, ], type="l", 
     ylab="", xlab="", main="Low education", ylim=c(-5,10))

plot(xaxis, dwage_m[2, ], type="l", 
     ylab="", xlab="", main="High school only", ylim=c(-5,10))

dwage_m[3,][dwage_m[3,] > 10] <- 10
dwage_m[3,][dwage_m[3,] < -5] <- -5
plot(xaxis, dwage_m[3, ], type="l", 
     ylab="", xlab="", main="2 to 3 years of higher education", ylim=c(-5,10))

dwage_m[4,][dwage_m[4,] > 10] <- 10
dwage_m[4,][dwage_m[4,] < -5] <- -5
plot(xaxis, dwage_m[4, ], type="l", 
     ylab="", xlab="", main="4 to 8 years of higher education", ylim=c(-5,10))

mtext("Number of simulations", side=1, line=-1.5, outer=T)
mtext("Parameter value", side=2, line=-1.5, outer=T)
dev.off()

# Daily wages
# Dependent variable dwage, 4 years later, men
pdf(file="boot_dwage_m_4.pdf")
par(mfrow = c(2, 2))
par(mar=c(6,4,4,1), xpd=T)
dwage_m[5,][dwage_m[5,] > 10] <- 10
dwage_m[5,][dwage_m[5,] < -10] <- -10
plot(xaxis, dwage_m[5, ], type="l", 
     ylab="", xlab="", main="Low education", ylim=c(-10,10))

plot(xaxis, dwage_m[6, ], type="l", 
     ylab="", xlab="", main="High school only", ylim=c(-10,10))

dwage_m[10,][dwage_m[10,] > 10] <- 10
dwage_m[10,][dwage_m[10,] < -10] <- -10
plot(xaxis, dwage_m[10, ], type="l", 
     ylab="", xlab="", main="2 to 3 years of higher education", ylim=c(-10,10))


dwage_m[8,][dwage_m[8,] > 10] <- 10
dwage_m[8,][dwage_m[8,] < -10] <- -10
plot(xaxis, dwage_m[8, ], type="l", 
     ylab="", xlab="", main="4 to 8 years of higher education", ylim=c(-10,10))

mtext("Number of simulations", side=1, line=-1.5, outer=T)
mtext("Parameter value", side=2, line=-1.5, outer=T)
dev.off()


# Daily wages
# Dependent variable dwage, 8 years later, men
pdf(file="boot_dwage_m_8.pdf")
par(mfrow = c(2, 2))
par(mar=c(6,4,4,1), xpd=T)
dwage_m[9,][dwage_m[9,] > 20] <- 20
dwage_m[9,][dwage_m[9,] < -20] <- -20
plot(xaxis, dwage_m[9, ], type="l", 
     ylab="", xlab="", main="Low education", ylim=c(-20,20))


plot(xaxis, dwage_m[10, ], type="l", 
     ylab="", xlab="", main="High school only", ylim=c(-20,20))

dwage_m[11,][dwage_m[11,] > 20] <- 20
dwage_m[11,][dwage_m[11,] < -20] <- -20
plot(xaxis, dwage_m[11, ], type="l", 
     ylab="", xlab="", main="2 to 3 years of higher education", ylim=c(-20,20))

dwage_m[12,][dwage_m[12,] > 20] <- 20
dwage_m[12,][dwage_m[12,] < -20] <- -20
plot(xaxis, dwage_m[12, ], type="l", 
     ylab="", xlab="", main="4 to 8 years of higher education", ylim=c(-20,20))

mtext("Number of simulations", side=1, line=-1.5, outer=T)
mtext("Parameter value", side=2, line=-1.5, outer=T)
dev.off()


# Number hours
# Dependent variable nbheur, 2 years later, men
pdf(file="boot_nbheur_m_2.pdf")
par(mfrow = c(2, 2))
par(mar=c(6,4,4,1), xpd=T)
nbheur_m[1,][nbheur_m[1,] > 200] <- 200
nbheur_m[1,][nbheur_m[1,] < -400] <- -400
plot(xaxis, nbheur_m[1, ], type="l", 
     ylab="", xlab="", main="Low education", ylim=c(-400,200))

plot(xaxis, nbheur_m[2, ], type="l", 
     ylab="", xlab="", main="High school only", ylim=c(-400,200))

plot(xaxis, nbheur_m[3, ], type="l", 
     ylab="", xlab="", main="2 to 3 years of higher education", ylim=c(-400,200))

nbheur_m[4,][nbheur_m[4,] > 200] <- 200
nbheur_m[4,][nbheur_m[4,] < -400] <- -400
plot(xaxis, nbheur_m[4, ], type="l", 
     ylab="", xlab="", main="4 to 8 years of higher education", ylim=c(-400,200))

mtext("Number of simulations", side=1, line=-1.5, outer=T)
mtext("Parameter value", side=2, line=-1.5, outer=T)
dev.off()


# Number hours
# Dependent variable nbheur, 4 years later, men
pdf(file="boot_nbheur_m_4.pdf")
par(mfrow = c(2, 2))
par(mar=c(6,4,4,1), xpd=T)
nbheur_m[5,][nbheur_m[5,] > 400] <- 400
nbheur_m[5,][nbheur_m[5,] < -400] <- -400
plot(xaxis, nbheur_m[5, ], type="l", 
     ylab="", xlab="", main="Low education", ylim=c(-400,400))

plot(xaxis, nbheur_m[6, ], type="l", 
     ylab="", xlab="", main="High school only", ylim=c(-400,400))

nbheur_m[7,][nbheur_m[7,] > 400] <- 400
nbheur_m[7,][nbheur_m[7,] < -400] <- -400
plot(xaxis, nbheur_m[7, ], type="l", 
     ylab="", xlab="", main="2 to 3 years of higher education", ylim=c(-400,400))


nbheur_m[8,][nbheur_m[8,] > 400] <- 400
nbheur_m[8,][nbheur_m[8,] < -400] <- -400
plot(xaxis, nbheur_m[8, ], type="l", 
     ylab="", xlab="", main="4 to 8 years of higher education", ylim=c(-400,400))

mtext("Number of simulations", side=1, line=-1.5, outer=T)
mtext("Parameter value", side=2, line=-1.5, outer=T)
dev.off()

# Number hours
# Dependent variable nbheur, 8 years later, men
pdf(file="boot_nbheur_m_8.pdf")
par(mfrow = c(2, 2))
par(mar=c(6,4,4,1), xpd=T)
nbheur_m[9,][nbheur_m[9,] > 300] <- 300
nbheur_m[9,][nbheur_m[9,] < -900] <- -900
plot(xaxis, nbheur_m[9, ], type="l", 
     ylab="", xlab="", main="Low education", ylim=c(-900,300))


plot(xaxis, nbheur_m[10, ], type="l", 
     ylab="", xlab="", main="High school only", ylim=c(-900,300))

nbheur_m[11,][nbheur_m[11,] > 300] <- 300
nbheur_m[11,][nbheur_m[11,] < -900] <- -900
plot(xaxis, nbheur_m[11, ], type="l", 
     ylab="", xlab="", main="2 to 3 years of higher education", ylim=c(-900,300))

nbheur_m[12,][nbheur_m[12,] > 300] <- 300
nbheur_m[12,][nbheur_m[12,] < -900] <- -900
plot(xaxis, nbheur_m[12, ], type="l", 
     ylab="", xlab="", main="4 to 8 years of higher education", ylim=c(-900,300))

mtext("Number of simulations", side=1, line=-1.5, outer=T)
mtext("Parameter value", side=2, line=-1.5, outer=T)
dev.off()
