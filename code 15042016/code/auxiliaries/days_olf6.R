require("dplyr")

days_olf6 <- function(dataset){
  
  # These helper functions are needed throughtout the code, in conjunction with the 
  # years_around_6_birth variable
  
  # MinPositive returns the minimum of a list of strictly positive numbers
  # MaxNegative returns the maximum of a list of negative numbers
  # IsNegative returns True if x is (stricly) negative
  
  MinPositive <- function(x){
    return(min(x[x > 0]))
  }
  
  MaxNegative <- function(x){
    return(max(x[x <= 0]))
  }
  
  IsNegative <- function(x, strict=T){
    if(strict==T){
      return(x < 0) 
    } else {
      return(x <= 0)
    }
  }
  
  
  # First we create the variable years_around_6_birth
  # This variable is very useful to identify when between have a kid (year 0)
  # as well as the years before the birth (which are going to be negative integers)
  # and the years after the birth (positive integers)
  
  print("creating years around birth")
  
  dataset$years_around_6_birth <- dataset$an - dataset$year_6_birth
  
  
  # Problem: parallel spells.
  # When the woman comes back and changes jobs, there are going to be several dates for debremu (start of pay)
  
  # To correct this, we group the data by identifier and year, and if there is more than one year
  # and for positive years after the birth we only keep the year were debremu (start of pay) is equal 
  # to the minimum. 
  
  print("creating years around birth")
  
  dataset %>% group_by(NNINOUV, an) %>% mutate(
    years_around_6_birth = as.numeric(ifelse(length(an) > 1 & years_around_6_birth > 0,
                                             ifelse(debremu==min(debremu), 
                                                    years_around_6_birth,
                                                    -99),
                                             years_around_6_birth))) -> dataset
  
  
  # We do the same for finremu (end of pay). This takes care of the case where a woman has a parallel spell 
  # when she gives birth.
  
  print("creating years around birth")
  
  dataset %>% group_by(NNINOUV, an) %>% mutate(
    years_around_6_birth = as.numeric(ifelse(length(an) > 1 & years_around_6_birth < 0,
                                             ifelse(finremu==max(finremu), 
                                                    years_around_6_birth,
                                                    -99),
                                             years_around_6_birth))) -> dataset
  
  # Now we create a dummy variable that equals 1 if a person never comes back after having a child
  # and 0 else. To check for that we see if every integer in years_around _6_birth is negative 
  
  print("creating right_cens")
  
  
  dataset %>% group_by(NNINOUV) %>% mutate(
    right_cens6 = as.numeric(ifelse(all(IsNegative(years_around_6_birth, strict=F)), 
                                    1,
                                    0))
  ) -> dataset
  
  # For people that leave and come back the same year, the number of days olf is equal to
  # 360 - sum(DP) for a given year. This is not entirely true however, as if there are parallel spells
  # we are going to overestimate the number of days that the person left. There doesn't seem to be a lot of 
  # cases were women come back the same year and change jobs afterwards, so this shouldn't be a very big issue
  # Maybe an elegant and simple solution will present itself, but for now, we only have this.
  
  print("creating days_olf_same_year6")
  
  
  dataset %>% group_by(NNINOUV, an) %>% mutate(
    days_olf_same_year6 = as.numeric(ifelse(sum(year_6_birth == an) >= 2, 
                                            360 - sum(DP),
                                            -99)
    )) -> dataset
  
  dataset %>% ungroup() -> dataset
  
  
  # For people that leave and come back later, we need to be more clever, and count the full years they're out.
  # For someone that came back the same year if the MinPositive is equal to 1, then the number of years out is 0.
  # else, it's going to be the MinPositive - MaxNegative - 1
  # For people that have a child before we observe them, we cannot say much, so we say that the number of years is -99
  
  print("creating years out")
  
  
  dataset %>% group_by(NNINOUV) %>% mutate(
    years_out6 = as.numeric(ifelse(
      length(which(year_6_birth == an)) > 1,
      ifelse(MinPositive(years_around_6_birth) == 1,
             0,
             MinPositive(years_around_6_birth) - MaxNegative(years_around_6_birth) - 1),
      ifelse(
        year_6_birth < min(an), 
        -99,
        MinPositive(years_around_6_birth) - MaxNegative(years_around_6_birth) - 1)))) -> dataset
  
  
  # Now we can count the number of days the person is out the year she leaves and the year she comes back
  
  print("creating days_olf")
  
  dataset %>% group_by(NNINOUV) %>% mutate(
    days_olf = as.numeric(ifelse(
      year_6_birth < min(an), 
      -99,
      ifelse(
        years_around_6_birth == MaxNegative(an - year_6_birth),
        360 - finremu,
        ifelse(
          years_around_6_birth == MinPositive(an - year_6_birth),
          ifelse(debremu==1, 0, debremu), 
          0)
      )))) -> dataset
  
  # This helper function only returns the sum of positive numbers in a list
  # This is useful, because in days_olf we have the numbe of days a person is a out the year she 
  # leaves and the year she comes back, and -99 everywhere else
  
  SumPositive <- function(x){
    return(sum(x[x>0]))
  }
  
  # Now we create another variable that is the sum of days_olf + the number of years converted to days
  print("creating days_olf6")
  
  
  dataset %>% group_by(NNINOUV) %>% mutate(days_olf6 = as.numeric(SumPositive(days_olf) + 360*(max(years_out6,0)))) -> dataset
  
  # Finally, we regroup everything. For people for which we don't observe a birth, we say that the number of days 
  # out is 0. For people that left and came back later, the number of days out is equal
  # to the variable days_olf6 that we computed above.
  # For people that left and came back the same year, the number of days olf is equal to days_olf_same_year6
  # plus the number of years out converted to days. This is usually 0, but if there is a case where there is 
  # parallell spell the year of the birth, followed by some years of olf, we consider that the years olf 
  # are for child rearing.
  
  # For people that leave and never come back, we simply count the number of years from 2010 to the birth of the 
  # child and convert that to days. We overestimate a bit the number of days in this manner, but by at most 360 days
  
  print("creating olf_due_6_birth")
  
  
  dataset %>% group_by(NNINOUV) %>% mutate(olf_due_6_birth = as.numeric(ifelse(year_6_birth < min(an),
                                                                               -99,
                                                                               ifelse(right_cens6 == 1,
                                                                                      (2010 - max(an))*360,
                                                                                      ifelse(all(IsNegative(days_olf_same_year6)),
                                                                                             max(days_olf6),
                                                                                             max(days_olf_same_year6) + 360*(max(years_out6, 0)) + max(ifelse(
                                                                                               years_around_6_birth == MinPositive(an - year_6_birth),
                                                                                               ifelse(debremu==1, 0, debremu), 
                                                                                               0)
                                                                                             )))))) -> dataset
  
  
  dataset %>% group_by(NNINOUV) %>% mutate(olf_due_6_birth = as.numeric(ifelse(
    right_cens6==1, min(60*360-max(age)*360, olf_due_6_birth), olf_due_6_birth
  ))) -> dataset
  
  print("removing unneeded variables")
  
  # Here we remove unneeded variables  
  dataset %>% select(-years_out6, -days_olf, -days_olf6, -days_olf_same_year6) -> dataset
  
  return(dataset)
  
}
