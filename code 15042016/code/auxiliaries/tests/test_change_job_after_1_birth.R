test_data <- readRDS("P:/Bruno/DADS-EDP/data/test_data.rds") 

test_that("Only positive values in years around birth",{
  person <- "N196710303074"
  test_data2 <- subset(test_data, NNINOUV == person)
  # Erase 1979
  test_data2 <- test_data2[-4, ]
  # Woman stopped working on the 150th day of 1978
  test_data2$finremu[3] <- 150
  test_data2$DP[3] <- 150
  # Woman started working of the 30th day of 1980
  test_data2$debremu[4] <-  30
  test_data2$DP[4] <- 330
  # Create one birth, erase other births
  test_data2$aen1 <- 1978
  # Day of birth
  test_data2$aen2 <- NA
  test_data2$AN01E90 <- NA 
  test_data2$AN02E90 <- NA
  test_data2$AN02E90 <- NA
  ordered_data <- order_births(test_data2, 3)
  ordered_data <- create_siret(ordered_data)
  ordered_data$SIRET[1:3] <- "9555026200001"
  ordered_data$SIRET[4] <- "5067200440001"
  ordered_data <- days_olf1(ordered_data)
  ordered_data$years_around_1_birth <- c(1,2,3,4)
  output <- changed_job_after_1_birth(ordered_data)
  true <- as.numeric(rep(NA, 4))
  expect_equal(true, output$cj_1_b)
})

test_that("Only negative values in years around birth",{
  person <- "N196710303074"
  test_data2 <- subset(test_data, NNINOUV == person)
  # Erase 1979
  test_data2 <- test_data2[-4, ]
  # Woman stopped working on the 150th day of 1978
  test_data2$finremu[3] <- 150
  test_data2$DP[3] <- 150
  # Woman started working of the 30th day of 1980
  test_data2$debremu[4] <-  30
  test_data2$DP[4] <- 330
  # Create one birth, erase other births
  test_data2$aen1 <- 1978
  # Day of birth
  test_data2$aen2 <- NA
  test_data2$AN01E90 <- NA 
  test_data2$AN02E90 <- NA
  test_data2$AN02E90 <- NA
  ordered_data <- order_births(test_data2, 3)
  ordered_data <- create_siret(ordered_data)
  ordered_data$SIRET[1:3] <- "9555026200001"
  ordered_data$SIRET[4] <- "5067200440001"
  ordered_data <- days_olf1(ordered_data)
  ordered_data$years_around_1_birth <- c(-1,-2,-3,-4)
  output <- changed_job_after_1_birth(ordered_data)
  true <- as.numeric(rep(NA, 4))
  expect_equal(true, output$cj_1_b)
})

test_that("Woman changed job after giving birth and coming back",{
  person <- "N196710303074"
  test_data2 <- subset(test_data, NNINOUV == person)
  # Erase 1979
  test_data2 <- test_data2[-4, ]
  # Woman stopped working on the 150th day of 1978
  test_data2$finremu[3] <- 150
  test_data2$DP[3] <- 150
  # Woman started working of the 30th day of 1980
  test_data2$debremu[4] <-  30
  test_data2$DP[4] <- 330
  # Create one birth, erase other births
  test_data2$aen1 <- 1978
  # Day of birth
  test_data2$aen2 <- NA
  test_data2$AN01E90 <- NA 
  test_data2$AN02E90 <- NA
  test_data2$AN02E90 <- NA
  ordered_data <- order_births(test_data2, 3)
  ordered_data$PCS4[1:3] <- "FOO"
  ordered_data$PCS4[4] <- "BAR"
  ordered_data <- days_olf1(ordered_data)
  output <- changed_job_after_1_birth(ordered_data)
  true <- rep(1, 4)
  expect_equal(true, output$cj_1_b)
})

test_that("Woman didn't changed job after giving birth and coming back",{
  person <- "N196710303074"
  test_data2 <- subset(test_data, NNINOUV == person)
  # Erase 1979
  test_data2 <- test_data2[-4, ]
  # Woman stopped working on the 150th day of 1978
  test_data2$finremu[3] <- 150
  test_data2$DP[3] <- 150
  # Woman started working of the 30th day of 1980
  test_data2$debremu[4] <-  30
  test_data2$DP[4] <- 330
  # Create one birth, erase other births
  test_data2$aen1 <- 1978
  # Day of birth
  test_data2$aen2 <- NA
  test_data2$AN01E90 <- NA 
  test_data2$AN02E90 <- NA
  test_data2$AN02E90 <- NA
  ordered_data <- order_births(test_data2, 3)
  ordered_data$PCS4[1:3] <- "FOO"
  ordered_data$PCS4[4] <- "FOO"
  ordered_data <- days_olf1(ordered_data)
  output <- changed_job_after_1_birth(ordered_data)
  true <- rep(0, 4)
  expect_equal(true, output$cj_1_b)
})

test_that("NA before birth",{
  person <- "N196710303074"
  test_data2 <- subset(test_data, NNINOUV == person)
  # Erase 1979
  test_data2 <- test_data2[-4, ]
  # Woman stopped working on the 150th day of 1978
  test_data2$finremu[3] <- 150
  test_data2$DP[3] <- 150
  # Woman started working of the 30th day of 1980
  test_data2$debremu[4] <-  30
  test_data2$DP[4] <- 330
  # Create one birth, erase other births
  test_data2$aen1 <- 1978
  # Day of birth
  test_data2$aen2 <- NA
  test_data2$AN01E90 <- NA 
  test_data2$AN02E90 <- NA
  test_data2$AN02E90 <- NA
  ordered_data <- order_births(test_data2, 3)
  ordered_data$PCS4[1:3] <- NA
  ordered_data$PCS4[4] <- "FOO"
  ordered_data <- days_olf1(ordered_data)
  output <- changed_job_after_1_birth(ordered_data)
  true <- as.numeric(rep(NA, 4))
  expect_equal(true, output$cj_1_b)
})

test_that("Only NA's",{
  person <- "N196710303074"
  test_data2 <- subset(test_data, NNINOUV == person)
  # Erase 1979
  test_data2 <- test_data2[-4, ]
  # Woman stopped working on the 150th day of 1978
  test_data2$finremu[3] <- 150
  test_data2$DP[3] <- 150
  # Woman started working of the 30th day of 1980
  test_data2$debremu[4] <-  30
  test_data2$DP[4] <- 330
  # Create one birth, erase other births
  test_data2$aen1 <- 1978
  # Day of birth
  test_data2$aen2 <- NA
  test_data2$AN01E90 <- NA 
  test_data2$AN02E90 <- NA
  test_data2$AN02E90 <- NA
  ordered_data <- order_births(test_data2, 3)
  ordered_data$PCS4[1:3] <- NA
  ordered_data$PCS4[4] <- NA
  ordered_data <- days_olf1(ordered_data)
  output <- changed_job_after_1_birth(ordered_data)
  true <- as.numeric(rep(NA, 4))
  expect_equal(true, output$cj_1_b)
})