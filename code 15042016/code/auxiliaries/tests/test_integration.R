# Integration test


test_data <- readRDS("P:/Bruno/DADS-EDP/data/test_data.rds") 



# Order births
test_data <- order_births(test_data)

# Remove weird lines
test_data <- remove_weird_lines(test_data)

# Create age at births
test_data <- age_at_births(test_data)

# Remove unneeded birth variables
test_data <- remove_unneeded_birth_variables(test_data)

# Create cumulkids
test_data <- cumulkids(test_data)

# Create nbkids
test_data <- nbkids(test_data)

# Create siret
test_data <- create_siret(test_data)

# Remove spells
test_data <- remove_spells(test_data)
