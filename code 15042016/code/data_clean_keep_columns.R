library("dplyr")

memory.limit(80000)

setwd("P:/Bruno/DADS-EDP/")

small_pandadsedp <- read.csv("data/pandadsedp.csv", header=TRUE, nrows=500000)


# Function to select useful columns
# Puts the name of the columns the user wants to keep in 
# the correct position in a vector
# NULL everywhere else

fun_keep_cols <- function(list_of_NULLs, dataset, name, force_factor=F){
  attach(dataset)
  index <- which(colnames(dataset)==deparse(substitute(name)))
    if(force_factor==T){
      list_of_NULLs[index] <- "factor"
    } else {
    if(class(name)=="integer"){
      list_of_NULLs[index] <- "numeric"
    } else {
      list_of_NULLs[index] <- class(name)
    }
  }
  detach(dataset)
  return(list_of_NULLs)
}

my_f <- function(dataset,name){
  attach(dataset)
  return(class(name))
}

# Select relevant variables
# Variables from the DADS
result_cols <- rep("NULL", length(ls(small_pandadsedp)))
result_cols <- fun_keep_cols(result_cols, small_pandadsedp, age)      # Age
result_cols <- fun_keep_cols(result_cols, small_pandadsedp, an)       # Year
result_cols <- fun_keep_cols(result_cols, small_pandadsedp, annai)    # Year of birth
#result_cols <- fun_keep_cols(result_cols, small_pandadsedp, APE40)        # aggregated economic activity of the firm since 2008 (NAF rev, 2)
result_cols <- fun_keep_cols(result_cols, small_pandadsedp, APET, force_factor=T)     # economic activity of the "etablissement" 
result_cols <- fun_keep_cols(result_cols, small_pandadsedp, APET2, force_factor=T)     # economic activity of the "etablissement" 
#result_cols <- fun_keep_cols(result_cols, small_pandadsedp, AVR)      # fringe benefit in €2007
#result_cols <- fun_keep_cols(result_cols, small_pandadsedp, BASCSGR)  # social contributions (French CSG) in €2007
result_cols <- fun_keep_cols(result_cols, small_pandadsedp, PCS4, force_factor=T)     # profession (since 1993)
$result_cols <- fun_keep_cols(result_cols, small_pandadsedp, PCS4_v2, force_factor=T) # Code PCS-ESE � 4 chiffres selon la m�thode de 2009
#result_cols <- fun_keep_cols(result_cols, small_pandadsedp, CE)         # Labour supply (C: Full-Time, P: Part-time, I: temporary, D: Work from home, A: Compensated unemployment)
result_cols <- fun_keep_cols(result_cols, small_pandadsedp, CE_red)     # "Corrected" Labour supply
result_cols <- fun_keep_cols(result_cols, small_pandadsedp, contrat_travail, force_factor=T) # Type of contract 
result_cols <- fun_keep_cols(result_cols, small_pandadsedp, debremu)    # remuneration starting date
result_cols <- fun_keep_cols(result_cols, small_pandadsedp, comR)       # commune of residence
result_cols <- fun_keep_cols(result_cols, small_pandadsedp, depR)       # department of residence
result_cols <- fun_keep_cols(result_cols, small_pandadsedp, comT)       # commune of work
result_cols <- fun_keep_cols(result_cols, small_pandadsedp, depT)       # department of work
#result_cols <- fun_keep_cols(result_cols, small_pandadsedp, domempl)    # domain of activity  
result_cols <- fun_keep_cols(result_cols, small_pandadsedp, DP)         # duration of pay in days
#result_cols <- fun_keep_cols(result_cols, small_pandadsedp, ENTPAN)     # date of entry in panel  
result_cols <- fun_keep_cols(result_cols, small_pandadsedp, ENTSIR)     # date of entry in current firm
result_cols <- fun_keep_cols(result_cols, small_pandadsedp, finremu)    # date ef end of pay
result_cols <- fun_keep_cols(result_cols, small_pandadsedp, nbheur)     # number of paid hours (since 1993)
result_cols <- fun_keep_cols(result_cols, small_pandadsedp, NETNETR)    # netnet wage in €2007
result_cols <- fun_keep_cols(result_cols, small_pandadsedp, NIC4)       # "etablissement" nic
result_cols <- fun_keep_cols(result_cols, small_pandadsedp, NNINOUV)    # individual identifier
#result_cols <- fun_keep_cols(result_cols, small_pandadsedp, SBR)        # gross wage in €2007
result_cols <- fun_keep_cols(result_cols, small_pandadsedp, SNR)        # net wage in €2007
result_cols <- fun_keep_cols(result_cols, small_pandadsedp, sx)         # gender (male==1)
result_cols <- fun_keep_cols(result_cols, small_pandadsedp, NIC4)       # establishment number
result_cols <- fun_keep_cols(result_cols, small_pandadsedp, sir)        # siren number (siren + nic4 gives the siret)
#result_cols <- fun_keep_cols(result_cols, small_pandadsedp, CDA)        # apprenticeship 
#result_cols <- fun_keep_cols(result_cols, small_pandadsedp, FPH)        # public servants in hospitals 
#result_cols <- fun_keep_cols(result_cols, small_pandadsedp, ADMIN)      # public servants in administrations
#result_cols <- fun_keep_cols(result_cols, small_pandadsedp, FPT)        # territorial public servants (in d�partements for example)
#result_cols <- fun_keep_cols(result_cols, small_pandadsedp, nbsa_et)    # number of workers in establishment 31/12/year
#result_cols <- fun_keep_cols(result_cols, small_pandadsedp, NES5)       # economical activity of establishment
#result_cols <- fun_keep_cols(result_cols, small_pandadsedp, PTT)        # public servant in mail
#result_cols <- fun_keep_cols(result_cols, small_pandadsedp, POIDS_EA_04)        # weights
#result_cols <- fun_keep_cols(result_cols, small_pandadsedp, POIDS_EA_05)        # weights
#result_cols <- fun_keep_cols(result_cols, small_pandadsedp, POIDS_EA_06)        # weights
#result_cols <- fun_keep_cols(result_cols, small_pandadsedp, POIDS_EA_07)
#result_cols <- fun_keep_cols(result_cols, small_pandadsedp, POIDS_EA_09)        # weights
#result_cols <- fun_keep_cols(result_cols, small_pandadsedp, POIDS_EA_10)        # weights
#result_cols <- fun_keep_cols(result_cols, small_pandadsedp, POIDS_EA_11)        # weights


# Variables from the EDP
# Birth year and months of the seven first children. Data from civil registries
result_cols <- fun_keep_cols(result_cols, small_pandadsedp, aen1)    
result_cols <- fun_keep_cols(result_cols, small_pandadsedp, aen2)    
result_cols <- fun_keep_cols(result_cols, small_pandadsedp, aen3)    
result_cols <- fun_keep_cols(result_cols, small_pandadsedp, aen4)    
result_cols <- fun_keep_cols(result_cols, small_pandadsedp, aen5)    
result_cols <- fun_keep_cols(result_cols, small_pandadsedp, aen6)    
result_cols <- fun_keep_cols(result_cols, small_pandadsedp, aen7)    
# result_cols <- fun_keep_cols(result_cols, small_pandadsedp, men1)    
# result_cols <- fun_keep_cols(result_cols, small_pandadsedp, men2)    
# result_cols <- fun_keep_cols(result_cols, small_pandadsedp, men3)    
# result_cols <- fun_keep_cols(result_cols, small_pandadsedp, men4)    
# result_cols <- fun_keep_cols(result_cols, small_pandadsedp, men5)    
# result_cols <- fun_keep_cols(result_cols, small_pandadsedp, men6)    
# result_cols <- fun_keep_cols(result_cols, small_pandadsedp, men7)    

# Birth year and months of the seven first children, data from the census (useful for women
# whose kids where born between 1982 - 1997)
result_cols <- fun_keep_cols(result_cols, small_pandadsedp, AN01E99)    
result_cols <- fun_keep_cols(result_cols, small_pandadsedp, AN01E90)
result_cols <- fun_keep_cols(result_cols, small_pandadsedp, AN02E99) 
result_cols <- fun_keep_cols(result_cols, small_pandadsedp, AN02E90)    #
result_cols <- fun_keep_cols(result_cols, small_pandadsedp, AN03E99)   
result_cols <- fun_keep_cols(result_cols, small_pandadsedp, AN03E90)    # Birth month of the third child
result_cols <- fun_keep_cols(result_cols, small_pandadsedp, AN04E99)    # Birth year of the fourth child
result_cols <- fun_keep_cols(result_cols, small_pandadsedp, AN04E90)    # Birth month of the fourth child
result_cols <- fun_keep_cols(result_cols, small_pandadsedp, AN05E99)    # Birth year of the fourth child
result_cols <- fun_keep_cols(result_cols, small_pandadsedp, AN05E90)    # Birth month of the fourth child
result_cols <- fun_keep_cols(result_cols, small_pandadsedp, AN06E99)    # Birth year of the fourth child
result_cols <- fun_keep_cols(result_cols, small_pandadsedp, AN06E90)    # Birth month of the fourth child
result_cols <- fun_keep_cols(result_cols, small_pandadsedp, AN07E99)    # Birth year of the fourth child
result_cols <- fun_keep_cols(result_cols, small_pandadsedp, AN07E90) 
# 
# result_cols <- fun_keep_cols(result_cols, small_pandadsedp, MN01E99, force_factor=T)    
# result_cols <- fun_keep_cols(result_cols, small_pandadsedp, MN01E90, force_factor=T)    # Birth month of the first child
# result_cols <- fun_keep_cols(result_cols, small_pandadsedp, MN02E99, force_factor=T)    # Birth year of the second child
# result_cols <- fun_keep_cols(result_cols, small_pandadsedp, MN02E90, force_factor=T)   # Birth month of the second child
# result_cols <- fun_keep_cols(result_cols, small_pandadsedp, MN03E99, force_factor=T)    # Birth year of the third child
# result_cols <- fun_keep_cols(result_cols, small_pandadsedp, MN03E90, force_factor=T)    # Birth month of the third child
# result_cols <- fun_keep_cols(result_cols, small_pandadsedp, MN04E99, force_factor=T)    # Birth year of the fourth child
# result_cols <- fun_keep_cols(result_cols, small_pandadsedp, MN04E90, force_factor=T)    # Birth month of the fourth child
# result_cols <- fun_keep_cols(result_cols, small_pandadsedp, MN05E99, force_factor=T)    # Birth year of the fourth child
# result_cols <- fun_keep_cols(result_cols, small_pandadsedp, MN05E90, force_factor=T)    # Birth month of the fourth child
# result_cols <- fun_keep_cols(result_cols, small_pandadsedp, MN06E99, force_factor=T)    # Birth year of the fourth child
# result_cols <- fun_keep_cols(result_cols, small_pandadsedp, MN06E90, force_factor=T)    # Birth month of the fourth child
# result_cols <- fun_keep_cols(result_cols, small_pandadsedp, MN07E99, force_factor=T)    # Birth year of the fourth child
# result_cols <- fun_keep_cols(result_cols, small_pandadsedp, MN07E90, force_factor=T)

result_cols <- fun_keep_cols(result_cols, small_pandadsedp, aem1)       # Year of first marriage
result_cols <- fun_keep_cols(result_cols, small_pandadsedp, mem1)       # Month of first marriage
#result_cols <- fun_keep_cols(result_cols, small_pandadsedp, jem1)       # Day of first marriage
result_cols <- fun_keep_cols(result_cols, small_pandadsedp, aem2)       # Year of second marriage
result_cols <- fun_keep_cols(result_cols, small_pandadsedp, mem2)       # Month of second marriage
#result_cols <- fun_keep_cols(result_cols, small_pandadsedp, jem2)       # Day of second marriage

#result_cols <- fun_keep_cols(result_cols, small_pandadsedp, DIP_99)    # Degree in 1999
#result_cols <- fun_keep_cols(result_cols, small_pandadsedp, DIP_10)    # Degree in 2010
result_cols <- fun_keep_cols(result_cols, small_pandadsedp, DIP_TOT)    # Degree at end of life
#result_cols <- fun_keep_cols(result_cols, small_pandadsedp, AFE82)      # Age at end of studies
#result_cols <- fun_keep_cols(result_cols, small_pandadsedp, POIDS EA)
#result_cols <- fun_keep_cols(result_cols, small_pandadsedp, annai)

# Now load whole dataset but only with needed columns
start <- Sys.time()
pandadsedp <- read.csv("data/pandadsedp.csv", header=TRUE, colClasses = result_cols) #, nrows=11299)
Sys.time() - start

rm(small_pandadsedp)
gc()

print("Writing data....")

start <- Sys.time()
saveRDS(pandadsedp, file = "data/clean_pandadsedp.rds")
Sys.time() - start

#rm(pandadsedp)
#gc()

# Create a data set only for men
start <- Sys.time()
pandadsedp %>% filter(sx==1) -> dadsedp_men
saveRDS(dadsedp_men, file = "data/dadsedp_men.rds")
Sys.time() - start


# Create a data set only for men
start <- Sys.time()
pandadsedp %>% filter(sx==0) -> dadsedp_women
saveRDS(dadsedp_women, file = "data/dadsedp_women.rds")
Sys.time() - start
