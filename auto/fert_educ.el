(TeX-add-style-hook
 "fert_educ"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-class-options
                     '(("svjour3" "smallcondensed")))
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("inputenc" "utf8") ("caption" "tableposition=top") ("eurosym" "gen") ("threeparttable" "flushleft")))
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperref")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperimage")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperbaseurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "nolinkurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "url")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "path")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "path")
   (TeX-run-style-hooks
    "latex2e"
    "tables/age_first_obs_2"
    "tables/age_last_obs_2"
    "tables/table_result_women_hwage_1child"
    "tables/table_result_women_nbheur_1child"
    "tables/table_result_women_hwage_2child"
    "tables/table_result_women_nbheur_2child"
    "tables/table_result_women_hwage_3child"
    "tables/table_result_women_nbheur_3child"
    "svjour3"
    "svjour310"
    "amsmath"
    "amssymb"
    "inputenc"
    "hyperref"
    "rotating"
    "tikz"
    "natbib"
    "caption"
    "booktabs"
    "eurosym"
    "graphicx"
    "float"
    "dirtytalk"
    "threeparttable"
    "setspace")
   (LaTeX-add-labels
    "intro"
    "data_method"
    "maritfoot"
    "econometric_method"
    "eq1"
    "did"
    "stats"
    "nb_id_in_year"
    "fertility_rate"
    "age_at_births_women"
    "age_at_births_men"
    "timing_of_births_women"
    "timing_of_births_men"
    "timing_of_births_women_by_educ"
    "timing_of_births_men_by_educ"
    "net_wage_nbkids_women"
    "net_wage_nbkids_men"
    "all_share_part_time"
    "nbheur_nbkids_women"
    "nbheur_nbkids_men"
    "groups"
    "results_discussion"
    "conclusion")
   (LaTeX-add-bibliographies
    "biblio"))
 :latex)

